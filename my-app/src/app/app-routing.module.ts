import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";

import { Project3Component } from "./pages/project-3/project-3.component";
import { Project2Component } from "./pages/project-2/project-2.component";
import { Project1Component } from "./pages/project-1/project-1.component";

const routes: Routes = [
  {
    path: '',
    redirectTo:'/about-me',
    pathMatch: 'full',
  },
  {
    path: 'about-me',
    component: Project2Component,
  },
  {
    path: 'pages-3',
    component: Project3Component,
  },
  {
    path: 'pages-2',
    component: Project2Component,
  },
  {
    path: 'pages-1',
    component: Project1Component,
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule { }
