import { NgModule } from "@angular/core";
import { BrowserModule } from "@angular/platform-browser";

import { AppComponent } from "./app.component";
import { AppRoutingModule } from "./app-routing.module";
import { AboutMeComponent } from "./pages/about-me/about-me.component";
import { MainComponent } from "./shared/components/main/main.component";
import { Project3Component } from "./pages/project-3/project-3.component";
import { Project2Component } from "./pages/project-2/project-2.component";
import { Project1Component } from "./pages/project-1/project-1.component";
import { AboutComponent } from "./pages/project-3/components/about/about.component";
import { OrderComponent } from "./pages/project-3/components/order/order.component";
import { HeaderAppComponent } from "./shared/components/header-app/header-app.component";
import { FeedbackComponent } from "./pages/project-3/components/feedback/feedback.component";
import { FooterappComponent } from "./pages/project-3/components/footerapp/footerapp.component";
import { HeaderappComponent } from "./pages/project-3/components/headerapp/headerapp.component";
import { NavigationComponent } from "./pages/project-3/components/navigation/navigation.component";
import { HolidaysTypeComponent } from "./pages/project-3/components/holidays-type/holidays-type.component";
import { MainProject2Component } from "./pages/project-2/components/main-project2/main-project2.component";
import { NewsProject2Component } from "./pages/project-2/components/news-project2/news-project2.component";
import { PhotoProject2Component } from "./pages/project-2/components/photo-project2/photo-project2.component";
import { SpecialOffersComponent } from "./pages/project-3/components/special-offers/special-offers.component";
import { AboutProject2Component } from "./pages/project-2/components/about-project2/about-project2.component";
import { BooksProject2Component } from "./pages/project-2/components/books-project2/books-project2.component";
import { WholeProject1Component } from "./pages/project-1/components/whole-project1/whole-project1.component";
import { HeaderProject2Component } from "./pages/project-2/components/header-project2/header-project2.component";
import { SkillsProject2Component } from "./pages/project-2/components/skills-project2/skills-project2.component";
import { SocialProject2Component } from "./pages/project-2/components/social-project2/social-project2.component";
import { SocialNavComponent } from "./pages/project-3/components/shared/components/social-nav/social-nav.component";

@NgModule({
  declarations: [
    AppComponent,
    MainComponent,
    AboutComponent,
    OrderComponent,
    AboutMeComponent,
    Project1Component,
    Project2Component,
    Project3Component,
    FeedbackComponent,
    FeedbackComponent,
    FeedbackComponent,
    FeedbackComponent,
    FeedbackComponent,
    FooterappComponent,
    SocialNavComponent,
    FooterappComponent,
    SocialNavComponent,
    FooterappComponent,
    SocialNavComponent,
    FooterappComponent,
    SocialNavComponent,
    HeaderAppComponent,
    HeaderappComponent,
    FooterappComponent,
    SocialNavComponent,
    NavigationComponent,
    HolidaysTypeComponent,
    MainProject2Component,
    NewsProject2Component,
    PhotoProject2Component,
    SpecialOffersComponent,
    AboutProject2Component,
    BooksProject2Component,
    WholeProject1Component,
    HeaderProject2Component,
    SkillsProject2Component,
    SocialProject2Component,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
